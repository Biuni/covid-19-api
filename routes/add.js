const express = require('express')
const router = express.Router()
const path = require('path')
const moment = require('moment')

const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync(`${path.dirname(__filename)}/../utils/db.json`)
const db = low(adapter)

/*
Route: /add
*/
router.post('/', (req, res, next) => {
  const country = req.body.country.split(' - ');
  db.get('list')
    .push({
      status: 0,
      title: req.body.title,
      description: req.body.description,
      country: country[1],
      countryCode: country[0],
      link: req.body.link,
      time: moment().format("Do MMMM YYYY"),
      category: req.body.category
    })
    .write()

  res.redirect('/?status=1')
})

module.exports = router
