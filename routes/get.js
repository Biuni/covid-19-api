const express = require('express')
const router = express.Router()
const path = require('path')

const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync(`${path.dirname(__filename)}/../utils/db.json`)
const db = low(adapter)

/*
  Route: /get
*/
router.get('/', (req, res, next) => {
  const activeList = db.get('list').filter({ status: 1 }).value()
  const categoriesList = db.get('categories').value()
  res.json({ categoriesList, activeList })
})

/*
  Route: /get/country/:countryId
*/
router.get('/country/:countryId', (req, res, next) => {
  const activeList = db.get('list').filter({ status: 1, countryCode: req.params.countryId }).value()
  const categoriesList = db.get('categories').value()
  res.json({ categoriesList, activeList })
})

module.exports = router
