const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  res.render('../view/home', {
    status: (req.query.status == 1) ? true : false
  })
})

module.exports = router
