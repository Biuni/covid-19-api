const express = require('express')
const bodyParser = require('body-parser')
const ip = require('./utils/ip').address()
const exphbs = require('express-handlebars')
const path = require('path')

const app = express()
const hbs = exphbs.create({
  defaultLayout: path.join(__dirname, '/view/layouts/main')
})

const view = require('./routes/view')
const get = require('./routes/get')
const add = require('./routes/add')
const notfound = require('./routes/notfound')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({ type: 'application/json' }))
app.use('/assets', express.static(path.join(__dirname, '/view/static')))

app.engine('handlebars', hbs.engine)
app.set('view engine', 'handlebars')

// Routing
app.use('/', view)
app.use('/get', get)
app.use('/add', add)
app.use(notfound)

const server = app.listen(process.env.PORT || 8080, () => {
  const host = server.address().address
  const port = server.address().port
  console.log(`Live on => ${host}:${port}\n`)
})

module.exports = app;
